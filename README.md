# ukfast/pokédex

**This project will be based primarily on your ability to fulfill the task 
requirements. Any potential design skills are a bonus, but usability, 
performance and security will be taken into account.**

## Introduction
This project provides a starting point which will allow you to create your own 
web-based encyclopedia based on the popular franchise Pokémon - also known as 
a pokédex.

## Project Requirements
To get started, you'll need the following:

 - PHP
 - [Composer](https://getcomposer.org/)
 - git
 
 You are free to use whatever PHP packages and front-end libraries that you 
 wish.

## Task Requirements
To order to complete this challenge, you MUST create a pokédex with minimal 
functionality. Your solution MUST allow the user to browse the full list of 
pokémon in a convenient manner, as well as offer some form of search 
functionality. Your solution MUST also display basic information for a 
specific pokémon, including:

 - At least one image of the pokémon
 - Name
 - Species
 - Height and weight
 - Abilities
 
A RESTful API is available at [Pokéapi](https://pokeapi.co/) which will 
provide you with all the data that you will need. You do not need to create 
an account nor authenticate in order to consume the API, however please 
be aware that this API is rate-limited.
 
To get started, we've given you a skeleton folder structure. It is advised 
that you spend no more than two to three hours on this assignment.
 
## Submission
To submit your solution, please fork this repository and provide us a link 
to your finished version.

## Copyright
All trademarks as the property of their respective owners.

## Matthew Ainge Comments on committed version
 - Full transparency: Full tracked working time on this was a bit short of 
 3.5 hours. I spent more time than necessary on the unnecessary logging class 
 and I got fussy over the pokemon selection window and general CSS
 - I use jQuery JS library, a Google Font API and the PHP library GuzzleHTTP.
 - With time in mind I did not tackle this as a TDD; the tests dir is vacant
 - With more planning, I would redesign some of these functions to eliminate
 the need for any knowledge of the API calls, nodes and commands, e.g. the 
 gallery class still needs to know the API uses 'results'
 - Styling was very rudimentary
 - Loading time of each gallery page isn't great.  Longer term, a full
 asynchronous load of each gallery item would be better.
 - Would have also preferred to make the API class implement an interface
 to make use of other API versions plausible without a lot of work etc.
 - Obvious lack of comments is obvious, I put some in when I could do so 
 quickly.

