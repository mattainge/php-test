<?php

require_once __DIR__ . '/../vendor/autoload.php';

$go = new \api\pokeapi();
$stop = new \GuzzleHttp\Client();
$log = new \core\logger();
$url = new \core\urlmasher();
$gallery = new \app\gallery();

?>


<!doctype html>
<html lang="en">
<head>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>

    <link rel="stylesheet" href="pokedex.css">
    <link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Pokédex</title>
</head>
<body>


<!-- Pokemon window -->
<div id="lightbox" class="fade"></div>
<div id="window" class="fade">
    <div class="nav"><span>X</span></div>
    <!-- Image-->
    <div id="pokeview"></div>
    <div id="details">
        <!-- DL/DT/DD data per Pokemon -->
        <dl>
            <dt>Name</dt>
            <dd id="name"></dd>
            <dt>Species</dt>
            <dd id="species"></dd>
            <dt>Height</dt>
            <dd id="height"></dd>
            <dt>Weight</dt>
            <dd id="weight"></dd>
            <dt>Abilities</dt>
            <dd id="abilities"></dd>
        </dl>

        <!-- Name -->
        <!-- Species -->
        <!-- Height/Weight -->
        <!-- Abilities -->
    </div>
</div>

<?php

/*
**  Gallery
*/

if( array_key_exists( 'search', $_POST ) ) {
    $gallery->search( $_POST['search'] ) ;
} else {
    $gallery->fetch( $url->getEndPointFromURL() );
}

// If we have something to show in our gallery (i.e. the API is working), then show it
if( $gallery ) {
    ?>

    <div id="gallery">

        <div class="nav">
            <?php
            if( $gallery->previous() ) {
                ?>
                <a href="<?php echo $gallery->previous() ?>">
                    Previous
                </a>
                <?php
            }
            ?>

            <form method="post" action="<?php echo $url->getLocalBaseURL() ; ?>">
                <input type="text" name="search" width="30">
                <input type="submit">

                <?php if( array_key_exists( 'search', $_POST ) ) {
                    ?>

                <p>
                    Search results for "<?php echo $_POST['search'] ; ?>".
                    <a href="<?php echo $url->getLocalBaseURL() ; ?>">Reset Search</a>
                </p>

                <?php } ?>
            </form>

            <?php
            if( $gallery->next() ) {
                ?>
                <a href="<?php echo $gallery->next() ?>">
                    Next
                </a>
                <?php
            }
            ?>
        </div>

        <?php

        foreach( $gallery->getResults() as $item ) {
            $pokemon = false;
            if( property_exists( $item, 'url' ) ) {
                $pokemon = new \app\pokemon( $item->url );
            }
            ?>
            <a href="<?php echo $item->url; ?>">
                <div class="entry">
                    <?php echo $pokemon ? '<img src="' . $pokemon->getFirstImage() . '">' : '?'; ?>
                    <span>
                        <?php echo ucfirst($pokemon->getName()); ?>
                    </span>
                </div>
            </a>
            <?php
        }

        ?>
    </div>
    <?php
}
?>

<script>
    function windowHide() {
        $('#window').removeClass('show');
        $('#lightbox').removeClass('show');
    }

    function windowShow() {
        $('#window').addClass('show');
        $('#lightbox').addClass('show');
    }

    $('#window .nav').click(function () {
        windowHide();
    });

    $('#lightbox').click( function(){
        windowHide() ;
    })
    $(document).on('keydown', function (event) {
        if (event.key === "Escape") {
            windowHide();
        }
    });

    $("#gallery > a").click(function () {
        let pokewindow = $('#window');
        let lightbox = $('#lightbox');

        $(pokewindow).removeClass('show');

        // Clear the 'forms' images
        $('#window #pokeview').html('');

        // Clear all list item contents
        $(pokewindow).find( 'dd' ).html( '' ) ;

        let url = $(this).attr('href');
        // url = 'http://whateverorigin.org/get?url=' + encodeURIComponent( url ) + '&callback=?' ;

        fetch(url, {
            mode: 'cors'
        })
            .then(response => {
                return response;
                throw new Error('Cannot fetch Pokemon details, sadface');
            })
            .then(response => response.json())
            .then(response => {
                console.log(response)

                // populate main vital stats
                pokewindow.find('#name').html(capitalize(response.name));
                pokewindow.find('#species').html(capitalize(response.species.name));
                pokewindow.find('#height').html(capitalize(response.height + ' decimetres'));
                pokewindow.find('#weight').html(capitalize(response.weight + ' hectograms'));
                pokewindow.find('#abilities').html( '<ul></ul>' ) ;

                for( let i in response.abilities ) {
                    let ability = response.abilities[i];
                    console.log( ability ) ;
                    pokewindow.find('#abilities ul').append( '<li>' + ability.ability.name + '</li>' ) ;
                }

                // Visualise all the forms
                for (let i in response.forms) {
                    let form = response.forms[i]

                    fetch(form.url)
                        .then(response => {
                            return response;
                            throw new Error('Cannot fetch Pokemon image details');
                        })
                        .then(response => response.json())
                        .then(response => {
                            //console.log( response ) ;
                            $('#window #pokeview').append('<figure><img src="'
                                + response.sprites.front_default
                                + '"><figcaption>'
                                + capitalize(response.name)
                                + '</figcaption></figure>');
                        });
                }
            })
            .then(() => {
                windowShow();
            })
            .catch(console.error);

        return false;
    });

    function capitalize(string) {
        return string.charAt(0).toUpperCase() + string.slice(1);
    }

</script>

<?php
$logged = $log->output();

if( count( $logged ) > 0 ) {
    ?>
    <div id="log"><b>Message log:</b>
        <?php

        foreach( $log->output() as $logMsg ) {
            ?>
            <div><b>Level <?php echo $logMsg[ 1 ]; ?></b>: <?php echo $logMsg[ 0 ]; ?></div>
            <?php
        }
        ?>
    </div>
    <?php
}
?>
</body>
</html>