<?php


/**
 * Created by PhpStorm.
 * User: Matthew Ainge
 * Date: 13/08/2019
 * Time: 01:17
 */

namespace app;


use \api\pokeapi;
use \core\pokeclass;
use \core\urlmasher;

class gallery extends pokeclass
{
    /**
     * @var object
     */
    private $data;


    /**
     * @var pokeapi
     */
    private $api;

    /**
     * @var urlmasher
     */
    private $url;

    public function __construct()
    {
        parent::__construct();

        $this->api = new pokeapi();
        $this->url = new urlmasher();
        $this->data = $this->api->getJson();

    }

    public function fetch( $url = null )
    {
        if( $url ) {
            $this->api->setURL( $url )->process();
        }

        $this->data = $this->api->getJson();

        return $this;
    }

    public function search( $search ) {
        $data = json_decode( json_encode( $this->api->search( $search ) ), 1 ) ;

        $filtered = array_filter( $data['results'], function( $v ) use ($search) {
            return ( strstr( $v['name'], $search ) ) ;
        }) ;

        $data['results'] = $filtered ;


       // die(var_dump( json_decode( json_encode($data, JSON_FORCE_OBJECT)) ) ) ;

        $this->data = json_decode( json_encode( $data ));
        return $this ;
    }

    public function getResults()
    {
        if( property_exists( $this->data, 'results' ) ) {
            return $this->data->results;
        }
        return false;
    }

    public function previous()
    {
        if( property_exists( $this->data, 'previous' ) && strlen( $this->data->previous ) > 0  ) {
            return $this->url->getURLFromEndPoint( $this->data->previous );
        }

        return false;
    }

    public function next()
    {
        if( property_exists( $this->data, 'next' ) & strlen( $this->data->next ) > 0 ) {
            return $this->url->getURLFromEndPoint( $this->data->next );
        }
        return false;
    }

    public function getCount()
    {
        if( property_exists( $this->data, 'count' ) ) {
            return $this->data->count;
        }
        return 0;
    }
}