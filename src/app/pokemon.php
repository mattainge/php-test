<?php


/**
 * Created by PhpStorm.
 * User: Matthew Ainge
 * Date: 13/08/2019
 * Time: 01:18
 */

namespace app;

use api\pokeapi;
use core\pokeclass;

class pokemon extends pokeclass
{
    /**
     * @var object
     */
    private $data ;


    /**
     * @var pokeapi
     */
    private $api ;


    /**
     * pokemon constructor.
     * @param $url  URL of the pokemon constructed provided by the API
     */
    public function __construct( $url )
    {
        parent::__construct() ;
        $this->api = new pokeapi();
        $this->api->setURL( $url )->process();
        $this->data = $this->api->getJson() ;
    }

    /**
     * Fetch the name
     * @return string|bool  Name if exists, else false
     */
    public function getName(){
        if( property_exists( $this->data, 'name' ) ) {
            return $this->data->name;
        }
        return false;
    }

    /**
     * Fetch the resource location for the pokemon's first form image
     * @return string|bool URL to the image
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getFirstImage() {

        foreach( $this->data->forms as $form ) {
            $pokeImage = $this->api
                ->setURL( $form->url )
                ->process()
                ->getJson() ;

            if( !property_exists( $pokeImage, 'sprites' ) ) {
                continue ;
            }

            if( !property_exists( $pokeImage->sprites, 'front_default' ) ) {
                continue ;
            }

            return $pokeImage->sprites->front_default ;
        }

        return false ;
    }

}