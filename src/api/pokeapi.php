<?php


/**
 * Created by PhpStorm.
 * User: Matthew Ainge
 * Date: 12/08/2019
 * Time: 22:15
 */

namespace api;

use core\pokeclass;
use GuzzleHttp\Client;


class pokeapi extends pokeclass
{
    /**
     * @var \GuzzleHttp\Client
     */
    private $client;

    /**
     * @var \Psr\Http\Message\ResponseInterface
     */
    private $res;

    /**
     * @var int
     */
    private $status;

    /**
     * @var object JSON data if successful
     */
    private $json;

    /**
     * @var string
     */
    private $url = 'https://pokeapi.co/api/v2/pokemon/';

    /**
     * @var int Location of selected pokemon ID in API endpoint
     */
    private $pokemonIDPathElement = 4;

    /**
     * @var array Default URL parameters used by the API for gallery and pokemon entries
     */
    private $urlParameters = [ 'offset', 'limit' ];

    /**
     * @var int
     */
    private $count;


    public function __construct()
    {
        parent::__construct();

        // Start at the full Pokemon index
        $this->client = new \GuzzleHttp\Client( [ 'defaults' => [ 'verify' => false ] ] );

        $this->process();

        if( property_exists( $this->json, 'count' ) ) {
            $this->count = $this->json->count;
        }

        if( $this->res->getStatusCode() !== 200 ) {
            $this->log->add( 'Pokedex connection failed' );
            return;
        }
    }

    /**
     * @return string
     */
    public function getAPIBaseURL()
    {
        return $this->url;
    }

    public function search( $search )
    {
        return $this->setURL( $this->url . '?offset=0&limit=' . $this->count )
            ->process()
            ->getJson();
    }

    /**
     * @return int
     */
    public function getAPIItemIDElement()
    {
        return $this->pokemonIDPathElement;
    }

    /**
     * @return array
     */
    public function getURLParameters()
    {
        return $this->urlParameters;
    }

    /**
     * @param $url
     * @return $this
     */
    public function setURL( $url )
    {
        if( filter_var( $url, FILTER_VALIDATE_URL ) ) {
            $this->url = $url;
        }
        return $this;
    }

    /**
     * @return $this
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function process()
    {
        if( $this->client instanceof Client ) {
            $this->res = $this->client->request( 'GET', $this->url, [] );

            $this->updateStatus()->convertResults();
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function updateStatus()
    {
        if( method_exists( $this->res, "getStatusCode" ) ) {
            $this->status = $this->res->getStatusCode();
        }
        return $this;
    }

    /**
     * @return $this
     */
    private function convertResults()
    {
        if( method_exists( $this->res, "getBody" ) ) {
            $this->json = json_decode( $this->res->getBody() );
        }
        return $this;
    }

    /**
     * @return string
     */
    public function getJson()
    {
        if( is_object( $this->json ) ) {
            return $this->json;
        }
    }
}