<?php


/**
 * Created by PhpStorm.
 * User: Matthew Ainge
 * Date: 13/08/2019
 * Time: 16:15
 */

namespace core;

use \api\pokeapi;

/**
 * Class urlmasher
 *
 * Takes an existing API URL as string value and forms a local app URL.
 * Also does the same in reverse, breaking down the current request, with parameters, to help create an API request.
 *
 * @package core
 */
class urlmasher extends pokeclass
{
    /**
     * @var array|mixed
     */
    private $path = [];

    /**
     * @var pokeapi
     */
    private $api;

    /**
     * @var array|mixed
     */
    private $parameters = [];

    /**
     * @var string
     */
    private $pokeapiURL;

    /**
     * @var array
     */
    private $apiURLParameters = [];


    public function __construct()
    {
        parent::__construct();

        /** @var  $api  Use for some base API defaults */
        $this->api = new pokeapi();

        $this->pokeapiURL = $this->api->getAPIBaseURL();

        $this->apiURLParameters = $this->api->getURLParameters();

        // Break current URL into path and parameters
        $urlParts = $this->breakURL( basename( $_SERVER[ 'REQUEST_URI' ] ) );

        $this->path = $urlParts[ 0 ];
        $this->parameters = $urlParts[ 1 ];
    }

    public function getPathElement( $i ) {
        return $this->path[ $i ] ;
    }

    /**
     * Return a parameter passed into the current query
     * @param $param string
     * @return string
     */
    public function getParameter( $param ){
        if( array_key_exists( $param, $this->parameters ) ){
            return $this->parameters[ $param ] ;
        }
        return false ;
    }

    public function getEndPointFromURL()
    {
        $url = $this->pokeapiURL;
        $i = 0;

        foreach( $this->parameters as $param => $value ) {
            if( in_array( $param, $this->apiURLParameters ) ) {

                $url .= ( strstr( $url, '?' ) ? '' : '?' ) . ( $i > 0 ? '&' : '' )  . $param . '=' . $this->parameters[ $param ];
                $i++;
            }
        }
        return $url;
    }

    public function getLocalBaseURL() {
        return sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['SERVER_PORT'] !== 443 && $_SERVER['SERVER_PORT'] !== 80 ? ':' . $_SERVER['SERVER_PORT'] : '' ,
            $_SERVER['REQUEST_URI']
        ) ;
    }

    public function getURLFromEndPoint( $url )
    {
        $urlParts = $this->breakURL( $url );

        // Get the current site URL with no parameters - we'll add valid params next, if any
        $newURL = sprintf(
            "%s://%s%s",
            isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != 'off' ? 'https' : 'http',
            $_SERVER['SERVER_NAME'],
            $_SERVER['SERVER_PORT'] !== 443 && $_SERVER['SERVER_PORT'] !== 80 ? ':' . $_SERVER['SERVER_PORT'] : '' ,
            $_SERVER['REQUEST_URI']
        ) ;

        $i = 0;


        foreach( $urlParts[ 1 ] as $param => $value ) {

            if( in_array( $param, $this->apiURLParameters ) ) {
                $newURL .= ( strstr( $newURL, '?' ) ? '' : '?' ) . ( $i > 0 ? '&' : '' ) . $param . '=' . $value;
                $i++;
            }
        }

        if( array_key_exists( $this->api->getAPIItemIDElement(), $urlParts[ 0 ] ) ) {
            $newURL .= ( strstr( $newURL, '?' ) ? '' : '?' ) . ( $i > 0 ? '&' : '' ) . 'pokemon=' . $urlParts[0][ $this->api->getAPIItemIDElement() ];
        }

        return $newURL ;
    }

    private function breakURL( $url )
    {
        // Break current URL into path and parameters
        $parts = explode( '?', $url );
        $params = [];


        if( array_key_exists( 1, $parts ) ) {
            parse_str( $parts[ 1 ], $params );
        }

        return [
            array_filter( explode( '/', parse_url( $parts[ 0 ], PHP_URL_PATH ) ) ),
            $params
        ];
    }
}