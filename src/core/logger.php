<?php


/**
 * Created by PhpStorm.
 * User: Matthew Ainge
 * Date: 12/08/2019
 * Time: 22:20
 */

namespace core;


class logger
{
    // All messages
    static $loggerContent = [] ;

    // Array of valid logging levels
    static $loggerLevels = [] ;

    // Default where no or invalid level is provided
    private $defaultLevel = 0 ;

    public function __construct()
    {
        self::$loggerLevels = [ $this->defaultLevel, 1, 2, 3, 4, 5 ] ;
    }

    /**
     * Log a new message
     * @param $text
     * @param $level  Optionally provide a level of 1-5.
     */
    public function add( $text, $level = null ){

        // If no level, set to default
        if( !$level ){
            $level = $this->defaultLevel ;
        }

        // If level is not the default value but is not valid, set to default and log the issue
        if( $level != $this->defaultLevel && !in_array( $level, self::$loggerLevels ) ) {
            $this->add( 'Unknown logging level provided *' . $level
                . '*, using default of ' . $this->defaultLevel . '.' , $this->defaultLevel ) ;
            $level = $this->defaultLevel ;
        }

        // Add the message
        self::$loggerContent[] = [ $text, $level ] ;
    }

    /**
     * @return array  An array of all messages
     */
    public function output(){
        return self::$loggerContent ;
    }
}